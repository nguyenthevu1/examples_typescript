"use strict";
exports.__esModule = true;
var classImplementsInterface_1 = require("./classImplementsInterface");
var documentOne = new classImplementsInterface_1.Invoice('henry', 'egg', 100000);
console.log(documentOne.print());
var lastT = function (arr) { return arr[arr.length - 1]; };
var l2 = lastT([1, 2, 3]);
var l3 = lastT(['d', 'e']);
console.log(l3);
