"use strict";
//class
exports.__esModule = true;
exports.Employee = void 0;
var Employee = /** @class */ (function () {
    function Employee(n, a, m) {
        this.name = n;
        this.age = a;
        this.male = m;
    }
    Employee.prototype.print = function () {
        return "Name: ".concat(this.name, ", Age: ").concat(this.age, ", Male: ").concat(this.male ? 'Nam' : 'Nu');
    };
    return Employee;
}());
exports.Employee = Employee;
var henry = new Employee('henry', 30, true);
var thevu = new Employee('thevu', 21, true);
var employees = [];
employees.push(thevu);
employees.push(henry);
employees.forEach(function (employee) { return console.log(employee.print()); });
