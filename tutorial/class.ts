//class

export class Employee {
    public name: string;
    private age: number;
    readonly male: boolean;

    constructor(n: string, a: number, m: boolean) {
        this.name = n;
        this.age = a;
        this.male = m;
    }

    print() {
        return `Name: ${this.name}, Age: ${this.age}, Male: ${
            this.male ? 'Nam' : 'Nu'
        }`;
    }
}

const henry = new Employee('henry', 30, true);
const thevu = new Employee('thevu', 21, true);

let employees: Employee[] = [];
employees.push(thevu);
employees.push(henry);

employees.forEach((employee) => console.log(employee.print()));
