import { hasPrint } from './interfaceForClass';
import { Invoice } from './classImplementsInterface';

const documentOne: hasPrint = new Invoice('henry', 'egg', 100000);

console.log(documentOne.print());

const lastT = <T>(arr: Array<T>) => arr[arr.length - 1];

const l2 = lastT([1, 2, 3]);
const l3 = lastT<string>(['d', 'e']);
console.log(l3);
