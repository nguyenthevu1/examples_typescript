// TYPE BASICS

let myName: string = 'thevu';
let age: number = 25;
let isStudent: boolean = false;

//FUNCTION
const square = (side: number) => side * side;
console.log(square(3));

let greet: Function;

greet = () => console.log('Hello');
greet();
