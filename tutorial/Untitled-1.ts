// bai 1
let outer: number = 1;

let inner;
const changeNumber = function () {
    const inner: number = -1;
    return (outer = 10);
};
inner = changeNumber();
outer = 1;
console.log(inner);

const string: string = `${22 / 7} is a decent approximation of π`;
console.log(string);

const mobyDick: string = `
            Call me Ishmael. Some years ago --
            never mind how long precisely -- having little
            or no money in my purse, and nothing particular
            to interest me on shore, I thought I would sail
            about a little and see the watery part of the
            world...`;

const evens = function () {
    let results: Array<number> = [];
    for (let i = 0; i < 10; i += 2) {
        results.push(i);
    }
    return results;
};
console.log(evens());

const yearsOld: object = {
    max: 10,
    ida: 9,
    tim: 11,
};

const ages = function () {
    let age: number;
    let results: Array<string> = [];
    for (let child in yearsOld) {
        age = yearsOld[child];
        results.push(`${child} is ${age}`);
    }
    return results;
};

console.log(ages());

const weatherReport = function (location: string): Array<string | number> {
    return [location, 72, 'Mostly Sunny'];
};
var [city, ...other] = weatherReport('Berkeley, CA');
// console.log();

class Animal {
    name: string;

    constructor(name: string) {
        this.name = name;
    }
    move(meters): string {
        return this.name + `move ${meters}m.`;
    }
}

class Horse extends Animal {
    speed: number;
    constructor(name: string, speed) {
        super(name);
        this.speed = speed;
    }
    move(): string {
        return super.move(this.speed);
    }
}

const tom: Animal = new Horse('Tommy the Palomino', 50);

console.log(tom.move(45));

class Teenager {
    say(speech): string {
        const words = speech.split(' ');

        const fillers: Array<string> = [
            'uh',
            'um',
            'like',
            'actually',
            'so',
            'maybe',
        ];
        let output: Array<string> = [];
        for (let index = 0; index < words.length; index++) {
            const word = words[index];
            output.push(word);
            if (index !== words.length - 1) {
                output.push(
                    fillers[Math.floor(Math.random() * fillers.length)],
                );
            }
        }
        return output.join(', ');
    }
}

const test: Teenager = new Teenager();

console.log(test.say('Hello Worl'));

const foods: Array<string> = ['toast', 'cheese', 'wine'];
for (let food = 0; food < foods.length; food++) {}

interface Pointlike {
    x: number;
    y: number;
}
interface Named {
    name: string;
}

function logPoint(point: Pointlike) {
    console.log('x = ' + point.x + ', y = ' + point.y);
}

function logName(x: Named) {
    console.log('Hello, ' + x.name);
}

const obj = {
    x: 0,
    y: 0,
    name: 'Origin',
};

logPoint(obj);
