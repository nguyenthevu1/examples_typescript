import express from 'express';
import connect from './db/connect';
import routes from './routes';
const app = express();

connect();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

routes(app);

app.listen(5000, () => {
    console.log(`Server listening at http://locaohost:${5000}`);
});
