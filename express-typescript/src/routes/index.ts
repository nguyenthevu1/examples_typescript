import { Express, Request, Response } from 'express';
import { createUserHandler } from '../controller/User.controller';
import { createUSerSessionHandler } from '../controller/Session.controller';

export default function (app: Express) {
    app.get('/healthcheck', (_: Request, res: Response) => {
        res.sendStatus(200);
    });

    // Register User
    //[POST /api/user]
    app.post('/api/user', createUserHandler);

    // Login User
    //[POST /api/session]
    app.post('/api/session', createUSerSessionHandler);
    //get the user's sessions
    //[get /api/sessions]

    //logout
    //[DELETE /api/sesions]

    //posts
    //[Get /api/posts]
}
