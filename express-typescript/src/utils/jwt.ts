import { Secret, SignOptions, sign } from 'jsonwebtoken';

const key: Secret = 'mySecret';

export function createToken(object: Object, options?: SignOptions | undefined) {
    return sign(object, key, options);
}
