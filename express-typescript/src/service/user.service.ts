import { DocumentDefinition } from 'mongoose';
import User, { UserDocument } from './../model/user.model';
import { omit } from 'lodash';
import bcrypt from 'bcrypt';

export async function createUser(input: DocumentDefinition<UserDocument>) {
    try {
        return await User.create(input);
    } catch (e: any) {
        throw new Error(e);
    }
}

export async function validatePassword({
    email,
    password,
}: {
    email: UserDocument['email'];
    password: string;
}) {
    const user = await User.findOne({ email });

    if (!user) {
        return false;
    }
    const isValid = await bcrypt.compareSync(password, user.password);

    if (!isValid) {
        return false;
    }
    return omit(user.toJSON(), 'password');
}
