import { sign } from 'jsonwebtoken';
import { Omit } from 'lodash';
import { LeanDocument } from 'mongoose';
import Session from '../model/session.model';
import { UserDocument } from 'src/model/user.model';
import { SessionDocument } from './../model/session.model';

export async function createSessionAsync(userId: string, userAgent: string) {
    const session = await Session.create({ user: userId, userAgent });

    return session.toJSON();
}

export function createAccessToken({
    user,
    session,
}: {
    user:
        | Omit<UserDocument, 'password'>
        | LeanDocument<Omit<UserDocument, 'password'>>;

    session: Omit<SessionDocument, 'password'> | LeanDocument<SessionDocument>;
}) {
    const accessToken = sign({ ...user, session: session._id }, 'mySecret', {
        expiresIn: '10m',
    });

    return accessToken;
}
