import { Request, Response } from 'express';
import {
    createAccessToken,
    createSessionAsync,
} from '../service/session.service';
import { validatePassword } from '../service/user.service';
import { createToken } from './../utils/jwt';

export async function createUSerSessionHandler(req: Request, res: Response) {
    const user = await validatePassword(req.body);
    if (!user) {
        return res.status(401).send('Invalid username or password');
    }

    const session = await createSessionAsync(
        user._id,
        req.get('user-agent') || '',
    );

    const accessToken = createAccessToken({ user, session });

    const refreshToken = createToken(session, {
        expiresIn: '24h',
    });

    return res.send({
        accessToken,
        refreshToken,
    });
}
