import mongoose from 'mongoose';

function connect() {
    const dbUri = 'mongodb://localhost:27017/examples_login';
    return mongoose
        .connect(dbUri, {
            useFindAndModify: true,
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true,
        })
        .then(() => {
            console.log('Database connected');
        })
        .catch((error) => {
            console.log('db error', error);
            process.exit(1);
        });
}
export default connect;
